<?php

return [

    // For shared database
    'brunoocto_upload_mysql' => [
        'driver' => 'mysql',
        'url' => !is_null(env('LINCKO_UPLOAD_DATABASE_URL')) ? env('LINCKO_UPLOAD_DATABASE_URL') : env('DATABASE_URL'),
        'host' => !is_null(env('LINCKO_UPLOAD_DB_HOST')) ? env('LINCKO_UPLOAD_DB_HOST') : env('DB_HOST', '127.0.0.1'),
        'port' => !is_null(env('LINCKO_UPLOAD_DB_PORT')) ? env('LINCKO_UPLOAD_DB_PORT') : env('DB_PORT', '3306'),
        'database' => !is_null(env('LINCKO_UPLOAD_DB_DATABASE')) ? env('LINCKO_UPLOAD_DB_DATABASE') : env('DB_DATABASE', 'forge'),
        'username' => !is_null(env('LINCKO_UPLOAD_DB_USERNAME')) ? env('LINCKO_UPLOAD_DB_USERNAME') : env('DB_USERNAME', 'forge'),
        'password' => !is_null(env('LINCKO_UPLOAD_DB_PASSWORD')) ? env('LINCKO_UPLOAD_DB_PASSWORD') : env('DB_PASSWORD', ''),
        'unix_socket' => !is_null(env('LINCKO_UPLOAD_DB_SOCKET')) ? env('LINCKO_UPLOAD_DB_SOCKET') : env('DB_SOCKET', ''),
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'prefix_indexes' => true,
        'strict' => true,
        'engine' => null,
        'options' => extension_loaded('pdo_mysql') ? array_filter([
            PDO::MYSQL_ATTR_SSL_CA => !is_null(env('LINCKO_UPLOAD_MYSQL_ATTR_SSL_CA')) ? env('LINCKO_UPLOAD_MYSQL_ATTR_SSL_CA') : env('MYSQL_ATTR_SSL_CA'),
        ]) : [],
    ],

    'brunoocto_upload_sqlite' => [
        'driver' => 'sqlite',
        'url' => '',
        // For static database (Read-only)
        'database' => __DIR__.'/../../database/database.sqlite',
        // For dynamic database (CRUD) linked to the server (not shared)
        // 'database' => isset($_SERVER['HTTP_HOST']) ? database_path('lincko_upload_database-'.\Str::slug($_SERVER['HTTP_HOST']).'-'.\App::environment().'.sqlite') : 'lincko_upload_database-'.\App::environment().'.sqlite',
        'prefix' => '',
        'foreign_key_constraints' => !is_null(env('LINCKO_UPLOAD_DB_FOREIGN_KEYS')) ? env('LINCKO_UPLOAD_DB_FOREIGN_KEYS') : env('DB_FOREIGN_KEYS', false),
    ],

];
