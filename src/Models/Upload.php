<?php

namespace Brunoocto\Upload\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Upload model
 *
 */
class Upload extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'brunoocto_uploads';

    /**
     * Guarded attributes
     * Array of attributes that we do not want to be mass assignable.
     *
     * @var array
     */
    protected $guarded = ["id_content"];

    protected $visible = ["filename" ];

    protected $appends = [""];
}
