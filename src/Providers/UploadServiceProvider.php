<?php

namespace Brunoocto\Upload\Providers;

use Dotenv\Dotenv;
use Illuminate\Support\ServiceProvider;
use Brunoocto\Upload\Contracts\UploadInterface;

/*
 * This importation specify the real service used.
 */
use Brunoocto\Upload\Services\UploadService;

class UploadServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Add SQLite database by merging configuration file
        $this->mergeConfigFrom(
            __DIR__.'/../../config/database.connections.php',
            'database.connections'
        );

        // Upload binding
        // The alias make sure that the Facade (Upload::) and the Maker will work
        $this->app->alias(UploadInterface::class, 'upload_interface');
        // The singleton (or bind) set any specification at instanciation
        $this->app->singleton(UploadInterface::class, UploadService::class);
        $this->app->singleton(UploadService::class, function () {
            $upload = new UploadService;
            // It helps to see in the response if the code has been throw this part (binding at instanciation)
            $upload->bind();
            return $upload;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Load environment variables specific to the library (default is .env)
        $dotenv = Dotenv::create(__DIR__.'/../../');
        $dotenv->load();

        // Load routes into the framework
        $this->loadRoutesFrom(__DIR__.'/../Routes/UploadRoutes.php');

        // Generate tables
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    }
}
