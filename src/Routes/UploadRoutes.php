<?php

/**
 * ROUTES STANDARDS
 * To work efficently with the application, we define some routes methods that must be applied.
 * For reference, https://jsonapi.org/format/ will be followed to insure uniformity of the whole application communication.
 *
 * REST CRUD operations (application/vnd.api+json):
 *     - POST: "Create"
 *     - GET: "Read"
 *     - PATH: "Update"
 *     - DELETE: "Delete"
 *
 * Any other non-REST actions:
 *     - PUT: "anything"
 *
 */

\Route::group([
    'middleware' => ['api',],
    'prefix' => 'brunoocto/upload',
    'namespace' => 'Brunoocto\Upload\Controllers',
], function () {
    // Model Upload
    \Route::post('uploads', 'UploadController@postUpload');
    // Divers actions
    \Route::put('dependency-injection', 'UploadController@putDependency');
    \Route::put('interface', 'UploadController@putInterface');
    \Route::put('facade', 'UploadController@putFacade');
    \Route::put('maker', 'UploadController@putMaker');
    \Route::put('test', 'UploadController@putTest');
});
