<?php

namespace Brunoocto\Upload\Controllers;

use Illuminate\Http\Request;

use Illuminate\Routing\Controller;
use Brunoocto\Upload\Models\Upload;
use Brunoocto\Upload\Services\UploadService;
use Brunoocto\Upload\Contracts\UploadInterface;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use League\Flysystem\Sftp\SftpAdapter;
use Illuminate\Http\File;
use Brunoocto\Filesystem\Services\FileService;

class UploadController extends Controller
{
    /**
     * Dependence injection
     * $upload will be instanciated according to the binding rule in the provider,
     * or simply by doing a "new UploadService" if no binding.
     * This method is suitable only for Service or Model that are specific tp the project itself,
     * and that we know that there won't be a replacement service possible, like a Model for a DB table.
     *
     * Pros:
     *   - Easy to setup
     *   - Easy to understand
     *   - Mock $upload in Unit test is simple.
     *
     * Cons:
     *   - It is fixed to a specific service. It makes difficult to switch to another library because we need to import the service itself each time we want to use it within the application.
     *
     * @param UploadService $upload
     * @return Response
     */
    public function putDependency(UploadService $upload)
    {
        return $upload->json('Dependency injection');
    }

    /**
     * Interface dependency injection
     * $upload will be instanciated according to the binding rule in the provider,
     * or simply by doing a "new UploadService" if no binding.
     * This method is suitable for shared services between project that we can easily mock.
     * And the contract (Interface) makes it easy to swap another service, we just need to change it in the provider.
     *
     * Pros:
     *   - Can design a shareable service for other projects
     *   - Very easy to swap to another service
     *   - Mock $upload in Unit test is simple.
     *
     * Cons:
     *   - Complex to understand how it works
     *   - Complex to setup
     *
     * @param UploadService $upload
     * @return Response
     */
    public function putInterface(UploadInterface $upload)
    {
        return $upload->json('Interface Dependency injection');
    }

    /**
     * Facade with Interface
     * A Facade is convenient to be called everywhere in the application.
     * It can be seen as a global instance (usually as a singleton) to access a logic.
     * This method is suitable for shared services between project with the advantage of a clean code.
     * Combined with Interface, it makes easier to swap the service.
     *
     * Pros:
     *   - Clean code
     *   - Easy to use
     *
     * Cons:
     *   - Complex to setup
     *   - Unusual because a Facade is a instanciated class, but methods are called like static ones but there are not.
     *   - Not design to be mocked for unit test
     *
     * @return Response
     */
    public function putFacade()
    {
        return \UploadAlias::json('Facade with Interface');
    }

    /**
     * Maker with Interface
     * Maker is the same as a Facade, it's slighty more verbose, but the way to use it is more natural.
     * Other comments are same as the Facade method thus.
     *
     * Pros:
     *   - Clean code
     *   - Easier to understand than Facade
     *   - Compare to Facade, methods are normally used.
     *
     * Cons:
     *   - More verbose than Facade
     *   - Not design to be mocked for unit test
     *
     * @return Response
     */
    public function putMaker()
    {
        return app()->make('upload_interface')->json('Maker with Interface');
    }

    /**
     * A route to test some code
     *
     * @return Response
     */
    public function postUpload(Request $request, Upload $upload)
    {
        $file = $request->file("file");
        Flysystem::put($file->getClientOriginalName(), file_get_contents($file));
        //$upload->save();

        return \UploadAlias::json('Create Upload', 201, 'You uploaded file :'.$file->getClientOriginalName());
    }

    /**
     * A route to test some code
     *
     * @return Response
     */
    public function putTest()
    {
        
        // Write your code here to test what you need

        return (new UploadService)->json('Controller for test only');
    }
}
