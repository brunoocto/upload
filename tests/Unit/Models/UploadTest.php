<?php

namespace Brunoocto\Upload\Tests\Unit\Models;

use Brunoocto\Upload\Models\Upload;
use Brunoocto\Upload\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UploadTest extends TestCase
{
    // Trait which does rollback to initial status any database modified during testing
    use RefreshDatabase;

    /**
     * Example of database insertion
     *
     * @return void
     */
    public function testCreateAUpload()
    {
        $count = Upload::count();
        
        $upload = factory(Upload::class)->create();

        // Check that that databe contains refreshly created text
        $this->assertDatabaseHas($upload->getTable(), [
            'text' => $upload->text,
        ]);

        // Check that the database has one more insert
        $this->assertCount($count+1, Upload::all());
    }
}
