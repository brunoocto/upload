<?php

namespace Brunoocto\Upload\Tests\Unit\Services;

use Brunoocto\Upload\Tests\TestCase;
use Brunoocto\Upload\Services\UploadService;

class UploadServicesTest extends TestCase
{
    /**
     * Test the format returned by the method "json"
     *
     * @return void
     */
    public function testJsonResponse()
    {
        // Check the type
        $upload = new UploadService;
        $json = $upload->json('test');
        $this->assertEquals('Illuminate\Http\JsonResponse', get_class($json));

        // Check the response is well formated
        $response = json_decode($json->content());
        $this->assertEquals('test', $response->data->{0});
        $this->assertEquals(false, $response->meta->binding);

        //Check the bind feature works
        $upload->bind();
        $json = $upload->json('test');
        $response = json_decode($json->content());
        $this->assertEquals(true, $response->meta->binding);
    }

    /**
     * Test the format returned by the method "json"
     *
     * @return void
     */
    public function testDotenv()
    {
        $this->assertEquals(false, env('UPLOAD_DB_FOREIGN_KEYS'));
    }
}
