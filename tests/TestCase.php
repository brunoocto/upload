<?php

namespace Brunoocto\Upload\Tests;

use Orchestra\Testbench\TestCase as TestbenchCase;
use Brunoocto\Upload\Providers\UploadServiceProvider;
use Brunoocto\Exception\Providers\ExceptionServiceProvider;

/**
 * TestCase for Service Provider
 */
class TestCase extends TestbenchCase
{
    /**
     * Setup
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        // Load fake data to the Database
        $this->withFactories(__DIR__.'/../database/factories');
    }
    
    /**
     * Get package providers.
     * @param  Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            UploadServiceProvider::class,
            ExceptionServiceProvider::class,
        ];
    }

    /**
      * Initialize environment
      * @return void
      */
    protected function getEnvironmentSetup($app)
    {
        // Configure Temporary Test database (not that it can be slow to run all tests within big project, but optimizations are possible)
        $app['config']->set('database.default', 'testdb');
        $app['config']->set('database.connections.testdb', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);

        /**
          *
          * Note that if you want to overwrite a configuration, do it on array, not its value
          *
          * This won't work:
          * $app['config']->set('filesystems.disks.local.root', storage_path('app'));
          *
          * This works:
          * $app['config']->set('filesystems.disks.local', [
          *   'driver' => 'local',
          *   'root' => storage_path('app'),
          * ]);
          *
          */
    }

    /**
      * Initialize Aliases
      * @param mixed $app
      * @return array
      */
    protected function getPackageAliases($app)
    {
        return [
            'UploadAlias' => 'Brunoocto\Upload\Facades\UploadFacade',
        ];
    }
}
