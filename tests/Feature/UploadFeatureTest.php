<?php

namespace Brunoocto\Upload\Tests\Feature;

use Brunoocto\Upload\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UploadFeatureTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Some checks that can be widely reused
     *
     * @return void
     */
    protected function commonCheck($response)
    {
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check the type of answer
        $response->assertHeader('Content-Type', 'application/vnd.api+json');
        
        // Check the Body structure
        $response->assertJsonStructure([
            'data',
            'errors',
            'meta' => [
                'message',
            ],
        ]);
    }

    /**
     * Test the dependency injection
     *
     * @return void
     */
    public function testUseDependencyInjection()
    {
        // Build the request
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('PUT', '/brunoocto/upload/dependency-injection');
        
        $this->commonCheck($response);

        // Check if the json contains some value
        $response->assertJson([
            'meta' => [
                'binding' => true,
            ],
        ]);

        //Check if the Body contains a String
        $response->assertSeeText('Dependency injection');
    }

    /**
     * Test the interface
     *
     * @return void
     */
    public function testUseInterface()
    {
        // Build the request
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('PUT', '/brunoocto/upload/interface');
        
        $this->commonCheck($response);

        // Check if the json contains some value
        $response->assertJson([
            'meta' => [
                'binding' => true,
            ],
        ]);

        //Check if the Body contains a String
        $response->assertSeeText('Interface Dependency injection');
    }

    /**
     * Test the facade
     *
     * @return void
     */
    public function testUseFacade()
    {
        // Build the request
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('PUT', '/brunoocto/upload/facade');
        
        $this->commonCheck($response);

        // Check if the json contains some value
        $response->assertJson([
            'meta' => [
                'binding' => true,
            ],
        ]);

        //Check if the Body contains a String
        $response->assertSeeText('Facade with Interface');
    }

    /**
     * Test the maker
     *
     * @return void
     */
    public function testUseMaker()
    {
        // Build the request
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('PUT', '/brunoocto/upload/maker');
        
        $this->commonCheck($response);

        // Check if the json contains some value
        $response->assertJson([
            'meta' => [
                'binding' => true,
            ],
        ]);

        //Check if the Body contains a String
        $response->assertSeeText('Maker with Interface');
    }

    /**
     * Test the maker
     *
     * @return void
     */
    public function testRunATest()
    {
        // Build the request
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('PUT', '/brunoocto/upload/test');
        
        $this->commonCheck($response);

        // Check if the json contains some value
        $response->assertJson([
            'meta' => [
                'binding' => false,
            ],
        ]);

        //Check if the Body contains a String
        $response->assertSeeText('Controller for test only');
    }

    /**
     * Test the maker
     *
     * @return void
     */
    public function testCreateAUpload()
    {
        // Build the request
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('POST', '/brunoocto/upload/uploads', [
            'data' => [
                'type' => 'upload',
                'attributes' => [
                    'text' => 'Some text'
                ],
            ],
        ]);
        
        $this->commonCheck($response);

        // Check if the json contains some value
        $response->assertJson([
            'meta' => [
                'binding' => true,
            ],
        ]);
        
        //Check if the Body contains a String
        $response->assertSeeText('Create Upload');
    }
}
