<?php

use Faker\Generator as Faker;
use Brunoocto\Upload\Models\Upload;

$factory->define(Upload::class, function (Faker $faker) {
    return [
        'text' => $faker->sentence,
    ];
});
